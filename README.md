# Adding IAM User and IAM role for Accessing the EKS cluster
1. `aws eks update-kubeconfig --region ap-south-1 --name eks-cluster --profile terraform`
2. Update the `reader-group-eks.yml` file to the eks cluster to create the cluster role and cluster role binding 
3. Create a IAM policy - AmazonEKSDeveloperPolicy - to reader permissions 
4. Create a developers group in IAM
5. Attach the policy to the Group
6. Add a developer user to the group
7. Configure the aws configure with developer profile
7. Verify the kubernetes context - `kubectl config view --minify`
8. Add the user arn in the auth by editing the configmap
  `kubectl edit -n kube-system configmap\aws-auth`
```
    apiVersion: v1
    kind: ConfigMap
    metadata:
    name: aws-auth
    namespace: kube-system
    data:
    mapUsers: |
    - userarn: arn:aws:iam::831955480324:user/pradeep
        username: pradeep
        groups:
        - reader
    mapRoles: |
        username: system:node:{{EC2PrivateDNSName}}
        rolearn: arn:aws:iam::831955480324:role/eks-node-group-nodes
        - groups:
        - system:bootstrappers
        - system:nodes 
```      
9. Switch the the developer profile in the kubeconfig
   `aws eks update-kubeconfig --region ap-south-1 --name eks-cluster --profile developer`
10. Verify the kubernetes context - `kubectl config view --minify`
11. Verify the setup - `kubectl auth can-i get pods`
12. Verify the setup - `kubectl auth can-i create pods`

## For Developer role
1. Create a IAM policy with Admin access to EKS cluster - AmazonEKSAdminPolicy
  - To view the nodes under the configuration tab in the management console
2. Create an IAM role - `eks-admin` and attach the above policy  
3. Run the command `aws iam get-role --role-name eks-admin --profile terraform`
   - Any IAM user from the account will be able to assume the role provided they have the appropriate policy in place
4. Create a IAM policy (AmazonEKSAssumePolicy) to assume the above role `eks-admin`
5. Create an IAM user and attach the above policy
6.  Run the command `aws configure --profile eks-admin`
7. Verify if the above user can assume the role
  `aws sts assume-role --role-arn arn:aws:iam::831955480324:role/eks-admin --role-session-name manager-session --profile manager`
8. Switch the context to the user who created the eks cluster  
`aws eks --region ap-south-1 update-kubeconfig --name eks-cluster`

## Adding the IAM role to the config-map
9. Edit the auth  `kubectl edit -n kube-system configmap\aws-auth`
  ```
  mapRoles: |
    - groups:
      - system:bootstrappers
      - system:nodes
      rolearn: arn:aws:iam::831955480324:role/eks-node-group-nodes
      username: system:node:{{EC2PrivateDNSName}}
    - groups:
      - system:masters
      rolearn: arn:aws:iam::831955480324:role/eks-admin
      username: eks-admin 
  ```
10. Update the config file in the local system - ~/.aws/config
11. EKS admin profile to assume the role by the manager
   ```
     [profile pradeep]
     role_arn: arn:aws:iam::831955480324:role/eks-admin
     source_profile = pradeep
   ```
12. Upadate the kuberetes context `aws eks --region ap-south-1 update-kubeconfig --name eks-cluster --profile eks-admin`
13. Check the context - `kubectl config view --minify`
14. Verify the setup - `kubectl auth can-i get pods`
15. Verify the setup - `kubectl auth can-i "*" "*"`


# Cluster autoscaler

1. Copy the openidconnect provider
2. In the IAM, create a Identity provider and the the openidconnect provider
3. Add the audience as sts.amazonaws.com
4. Navigate to the IAM policies and create the policy for the autoscaler to adjust the number of worker nodes `AmazonEKSClusterAutoscalerPolicy`
5. Create a new Role with web identity and attach the above policy `AmazonEKSClusterAutoscalerRole`
6. Update the trust relationshop for the `AmazonEKSClusterAutoscalerRole` role
```
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Principal": {
				"Federated": "arn:aws:iam::831955480324:oidc-provider/oidc.eks.ap-south-1.amazonaws.com/id/08542D07CF84F4C9C16EDC17273048BF"
			},
			"Action": "sts:AssumeRoleWithWebIdentity",
			"Condition": {
				"StringEquals": {
					"oidc.eks.ap-south-1.amazonaws.com/id/08542D07CF84F4C9C16EDC17273048BF:sub": "system:serviceaccount:kube-system:cluster-autoscaler"
				}
			}
		}
	]
}
```
7. Create the service account with the `role-arn` of the `AmazonEKSClusterAutoscalerRole` `arn:aws:iam::831955480324:role/AmazonEKSClusterAutoscalerRole`
8. Update the `Cluster Role`, `Role` and `RoleBindings` and the `Cluster Autoscaler` 
9. Update the version of the EKS in the container image and the cluster name
10. Deploy the manifest file

# Affinity and AntiAffinity
## NodeSelector
- Add label to the Node when creating the cluster
- Chech the labels - `kubectl get nodes --show-labels`
- Add the label manually for one of the node `kubectl label node <node-name> disk=ssd`
- Check if the nodes have the label
- Create a deployment file with the `NodeSelector` in the `PodSpec` definition
- Apply the manifest file and check the pods `kubectl get pods -o wide`
- Test with incorrect label in the pod definition - the pod status will be in pending state
- Run the describe command to troubleshoot the issue

## NodeAffinity - with hard requirement
- More expressive way to specify the constraints 
- `requiredDuringSchedulingIgnoreDuringExcecution`
- supported operators - `IN, NOTIN, Exists, DoesNotExists, Gt, Lt`

## NodeAffinity - with soft requirement
- Soft ensures that the scheduler will still schedule the pods on another node if the criteria is not met
- `preferredDuringSchedulingIgnoreDuringExcecution`
- additional `weight` attribute
## PodAffinity - Pod selector level 
 - similar to Node affinity

## PodAntiAffinity - Pod selector level 
 - similar to Node antiaffinity

## Using Secrets with AWS Secrets Manager
1. Create a Secret in AWS Secrets Manager
   `DB_USERNAME = SA`
   `DB_PASSWORD= welcome` 
2. Select the `arn:aws:secretsmanager:ap-south-1:831955480324:secret:dev/db/credentials-9ebL53` of the secret
3. Copy the OpenID connect provider from the EKS console 
4. Click of Identity provider from IAM dashboard
5. Add a new provider 
6. Select OpenID connect
7. Paste the URL of the OpenID connect provider
8. Verify the thumbprint
9. Set the audience as sts.amazonaws.com - To be later updated with IAM Role trust relationship
10. Create an IAM Policy to read the secrets from the secrets manager - `DBCredentialsReadAccessPolicy`
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "secretsmanager:GetSecretValue",
            "Resource": "arn:aws:secretsmanager:ap-south-1:831955480324:secret:dev/db/credentials-9ebL53"
        }
    ]
}
```
11. Create an Identity role for K8s service account with `Web Identity`
12. Edit the trust relationship with `sub` and the service account `system:serviceaccount:production:nginx`
13. Associate the above IAM role  with the service account
14. Deploy the `namespace` and the `service-account` manifests file
    ` kubectl get sa -n production`
15. The service account now has access to the secret
16.  Install the kubernetes secrets store csi driver which integrates secrets stores with K8s via yaml (secretproviderclasses-crd and secretproverclasspodstatus)
17. Verify if the crds are installed `kubectl get crds`
18. The secret store csi driver should be run on each of the worker node using the `daemonset`
19.  Check if there are any errors ` kubectl logs -n kube-system -f -l app=secrets-store-csi-driver`
20. Install AWS secrets and Configuration provider
21. The AWS provider for secret store csi driver allows us to make the secret store in secret manager and parameter inside patameter store 
22. Apply the yaml definitions - `kubectl apply -f aws-provider-installer` 
23. Check all the pods - `kubectl get pods -n kube-system` to verify the `secret store csi driver` running on all the nodes (daemonset)
24. Check the logs - `kubectl logs -n kube-system -f -l app=csi-secrets-store-provider-aws`
25. Create the `secret provider` class which will map the secret in the secrets manager with the kubernetes provider
26. Should be deployed in the same namespace (`production`)
27. Deploy all the manifests file
28. Get all the secrets in the `production` namespace - `kubectl get secrets -n production`
29. Login to the nginx pod `kubectl exec -it <nginx-pod-name> -- \bin\bash`
30. Check the secrets `cat /mnt/api-token/secret-token` and `echo $API_TOKEN`

# Using PVC with EKS
- Either use EBS volume ot EFS as persistent storage

## EBS volume
https://github.com/kubernetes-sigs/aws-ebs-csi-driver/
- Download the IAM policy
 `https://github.com/kubernetes-sigs/aws-ebs-csi-driver/blob/v1.12.1/docs/example-iam-policy.json`

-  Create an IAM Policy with the command
 - `aws iam create-policy --policy-name Amazon_EBS_CSI_Driver_Policy --policy-document file://example-iam-policy.json`
- Install the csi driver from the repo `kubectl apply -f kubectl apply -k "github.com/kubernetes-sigs/aws-ebs-csi-driver/deploy/kubernetes/overlays/stable/?ref=release-1.12"`
-  Fetch the `oidc provider` url
-  Create the trust policy file
   `sub`, `system:serviceaccount:kube-system:ebs-csi-controller-sa`
-  Create a `ebs-csi-driver` role and attach the policy
-  Deploy the `ebs-csi` driver with the below command
`kubectl apply -f https://github.com/kubernetes-sigs/aws-ebs-csi-driver/tree/v1.12.1/deploy/kubernetes/overlays/stable` 
- 

# Accessing the AWS services from K8s securely is by using OpenID connect provider

And establish the trust between AWS IAM and Kubernetes RBAC system

IAM roles for service account 

Steps
1. Copy the OpenID connect provider from the EKS console 
2. Click of Identity provider from IAM dashboard
3. Add a new provider 
4. Select OpenID connect
5. Paste the URL of the OpenID connect provider
6. Verify the thumbprint
7. Set the audience as sts.amazonaws.com - To be later updated with IAM Role trust relationship
8. Complete the setup 

Create an IAM policy for permission for AWS Load balancer controller 
https://github.com/kubernetes-sigs/aws-load-balancer-controller
  - Under to docs/install/iam_policy.json
1. In the IAM section, create a new policy - AWSLoadBalancerController
2. Create an IAM role to attach the above policy 
3. Create a Web identity 
4. Select the OpenID connect provider and audience 
5. Attach the IAM policy to the role - aws-load-balancer-controller 
6. Navigate to the trust relationship and edit trust policy 
7. Replace aud with sub 
8. Value with the service account - system:serviceaccount:kube-system:aws-load-balancer
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::831955480324:oidc-provider/oidc.eks.ap-south-1.amazonaws.com/id/C5616185A7816637ABA107393AF5A559"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringEquals": {
                    "oidc.eks.ap-south-1.amazonaws.com/id/C5616185A7816637ABA107393AF5A559:aud": "sts.amazonaws.com",
                    "oidc.eks.ap-south-1.amazonaws.com/id/C5616185A7816637ABA107393AF5A559:sub": "system:serviceaccount:kube-system:aws-load-balancer-controller"
                }
            }
        }
    ]
}




